<?php

define('USERS_TABLE', 'phpbb_users');
define('TOPICS_TABLE', 'phpbb_topics');
define('POSTS_TABLE', 'phpbb_posts');

define('ANONYMOUS', 1);
define('USER_NORMAL', 2);
define('USER_FOUNDER', 3);

class user
{
	public $data = array(
		'user_lang' => 'en',
		'user_id' => 2,
		'session_id' => 'mock_session_id',
		'username' => 'TestUser',
		);

	public $lang = array(
		'LOGOUT_USER' => 'Logout (disabled)'
	);
	
	public function session_begin()
	{
		return true;
	}

	public function setup()
	{
		return true;
	}

	public function add_lang($lang_file)
	{
		return true;
	}

	public function __get($field_name)
	{
		return $this->data[$field_name];
	}
}

$user = new user();

class auth
{
	public function acl($sth)
	{
		return true;
	}
}

$auth = new auth();

class db
{
	private $db_file;
	
	function __construct($db_file_path)
	{
		$this->db_file = new SQLite3($db_file_path, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);;
	}
	
	public function sql_query($query)
	{
		if($res = $this->db_file->query($query))
		{
			return $res;
		}
		else
		{
			die($query . "\n -> " . $this->db_file->lastErrorMsg());
		}
	}
	
	public function sql_fetchrow($res)
	{
		return $res->fetchArray(SQLITE3_ASSOC);
	}

	public function sql_escape($txt)
	{
		return SQLite3::escapeString($txt);
	}
	
	public function get_any_char()
	{
		return chr(0) . '%';
	}

	public function sql_like_expression($expression)
	{
		// Unlike LIKE, GLOB is unfortunately case sensitive.
		// We only catch * and ? here, not the character map possible on file globbing.
		$expression = str_replace(array(chr(0) . '_', chr(0) . '%'), array(chr(0) . '?', chr(0) . '*'), $expression);

		$expression = str_replace(array('?', '*'), array("\?", "\*"), $expression);
		$expression = str_replace(array(chr(0) . "\?", chr(0) . "\*"), array('?', '*'), $expression);

		return 'GLOB \'' . $this->sql_escape($expression) . '\'';
	}
}

$db = new db(__DIR__ . '/db.sqlite');

function append_sid($url, $params = false)
{
	global $user;

	return $url . ($params !== false ? $params : '') . '&amp;sid=' . $user->data['session_id'];
}

function censor_text($text)
{
	return $text;
}

function smiley_text($text)
{
	return $text;
}

function bbcode_nl2br($text)
{
	return nl2br($text);
}

class bbcode
{
	function __construct($bitfield)
	{
	}
	
	function bbcode_second_pass($text, $uid, $bitfield)
	{
		return $text;
	}
}

?>
