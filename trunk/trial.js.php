<?php
/*
Ace Attorney Online - Export trial data to Javascript

*/

if(!defined('ALREADY_INCLUDED'))
{
	include('common_render.php');
	
	header('Content-type: text/javascript; charset=UTF-8');
}

$trial_id = intval($_GET['trial_id']);

?>
/*
Ace Attorney Online - Trial data module

*/

//MODULE DESCRIPTOR
Modules.load(new Object({
	name : 'trial',
	dependencies : [],
	init : function() {}
}));


//INDEPENDENT INSTRUCTIONS


//EXPORTED VARIABLES
<?php

//Language of the current user
echo "var user_language = '" . getInstalledLanguage(UserDataHandler::getCurrentUser()->getLanguage()) . "';";

$trial = TrialMetadataHandler::getHandlerFor($trial_id);

if($trial == null OR !$trial->canUserRead(UserDataHandler::getCurrentUser()))
{
	echo 'var trial_information;';
	echo 'var initial_trial_data;';
}
else
{
	if(isset($_GET['trial_backup_type']) AND isset($_GET['trial_backup_date']))
	{
		$backups = ($_GET['trial_backup_type'] == 'auto') ? $trial->getAutoBackups() : $trial->getManualBackups();
		
		if(array_key_exists($_GET['trial_backup_date'], $backups))
		{
			$trial_file = $backups[$_GET['trial_backup_date']];
		}
	}
	else
	{
		$trial_file = $trial->getTrialFile();
	}
	
	if($trial_file == null)
	{
		echo 'var trial_information;';
		echo 'var initial_trial_data;';
	}
	else
	{
		$sequence = $trial->getParentSequence();
		
		$trial_information = array(
			'id' => $trial->getId(),
			'title' => $trial->getTitle(),
			'author_id' => $trial->getAuthor()->getId(),
			'author' => $trial->getAuthor()->getName(),
			'language' => $trial->getLanguage(),
			'sequence' => $sequence == null ? null : $sequence->export(),
			'can_read' => true,
			'can_write' => $trial->canUserWriteContents(UserDataHandler::getCurrentUser()),
			'last_edit_date' => $trial_file->getLastModificationDate(),
			'format' => $trial_file->getFormat()
		);
		
		if($trial_file->getFilePath() != null)
		{
			$trial_information['file_path'] = $trial_file->getFilePath();
		}
		
		echo 'var trial_information = JSON.parse("' . escapeJSON(json_encode($trial_information)) . '");' . "\n";
		echo 'var initial_trial_data = JSON.parse("' . escapeJSON($trial_file->getTrialData()) . '");';
	}
}
//print_r($trial_file->query->getTrialData(TRIALDATA_AS_ARRAY));
?>
var trial_data = initial_trial_data;


//EXPORTED FUNCTIONS


//END OF MODULE
Modules.complete('trial');
